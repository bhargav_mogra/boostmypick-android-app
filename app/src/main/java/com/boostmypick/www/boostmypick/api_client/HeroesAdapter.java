package com.boostmypick.www.boostmypick.api_client;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.boostmypick.www.boostmypick.R;
import com.boostmypick.www.boostmypick.SelectableViewHolder;
import com.boostmypick.www.boostmypick.api_client.mappers.Hero;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class HeroesAdapter extends RecyclerView.Adapter<HeroesAdapter.ViewHolder> {
    private List<Hero> heroes;
    private OnHeroSelectedListener listener;
    private List<Hero> selectedHeroes = new ArrayList<>(5);

    public HeroesAdapter(List<Hero> heroes, OnHeroSelectedListener listener) {
        this.heroes = heroes;
        this.listener = listener;
    }

    public int getPositionFor(CharSequence input) {
        for (int i = 0; i < heroes.size(); i++) {
            String name = heroes.get(i).getName().toLowerCase();
            if (name.startsWith(input.toString())) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hero_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Hero item = heroes.get(position);
        if (item == null)
            return;
        holder.item = item;
        holder.heroTitle.setText(item.getName());
        Glide.with(holder.heroImage.getContext()).load(item.getImageUrl()).into(holder.heroImage);
    }

    @Override
    public int getItemCount() {
        return heroes.size();
    }

    public List<Hero> getSelectedHeroes() {
        return selectedHeroes;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, SelectableViewHolder {
        public final View view;
        public final ImageView heroImage;
        public final TextView heroTitle;
        public Hero item;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            heroImage = (ImageView) itemView.findViewById(R.id.image_hero);
            heroTitle = (TextView) itemView.findViewById(R.id.txt_hero_title);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (item != null) {
                if (item.isSelected()) {
                    item.setSelected(false);
                    if (selectedHeroes.contains(item)) {
                        selectedHeroes.remove(item);
                    }
                    listener.onHeroUnSelected(item);
                } else {
                    if (selectedHeroes.size() >= 5) {
                        listener.onLimitExceeded();
                        return;
                    } else {
                        selectedHeroes.add(item);
                    }
                    item.setSelected(true);
                    listener.onHeroSelected(item);
                }
                notifyItemChanged(heroes.indexOf(item));
            }
        }

        @Override
        public boolean isSelected() {
            return item != null && item.isSelected();
        }
    }
}
