package com.boostmypick.www.boostmypick;

public interface SelectableViewHolder {
    boolean isSelected();
}
