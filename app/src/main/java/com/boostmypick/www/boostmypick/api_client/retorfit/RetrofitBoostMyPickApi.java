package com.boostmypick.www.boostmypick.api_client.retorfit;

import com.boostmypick.www.boostmypick.api_client.ApiCallback;
import com.boostmypick.www.boostmypick.api_client.ApiException;
import com.boostmypick.www.boostmypick.api_client.IBoostMyPickApi;
import com.boostmypick.www.boostmypick.api_client.mappers.BestHero;
import com.boostmypick.www.boostmypick.api_client.mappers.Hero;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import retrofit2.Response;

public class RetrofitBoostMyPickApi implements IBoostMyPickApi {
    private RetrofitApiInterface apiInterface;

    public RetrofitBoostMyPickApi(String baseUrl) {
        this.apiInterface = new RetrofitModule(baseUrl).retrofitApiInterface();
    }

    @Override
    public List<Hero> getHeroes() throws IOException {
        Response<Hero[]> response = apiInterface.getHeroes().execute();
        if (response.isSuccessful()) {
            return Arrays.asList(response.body());
        } else {
            throw new ApiException(response.code());
        }
    }

    @Override
    public void getHeroes(ApiCallback<Hero[]> callback) {
        apiInterface.getHeroes().enqueue(new RetrofitCallback<Hero[]>(callback));
    }

    public List<BestHero> getBestHeroes(int[] heroIds, int limit) throws IOException {
        Map<String, String> options = new TreeMap<>();
        options.put("id", getCommaSeparatedHeroIds(heroIds));
        options.put("limit", String.valueOf(limit));
        Response<List<BestHero>> response = apiInterface.getBestHeroes(options).execute();
        if (response.isSuccessful()) {
            return response.body();
        } else {
            throw new ApiException(response.code());
        }
    }

    public void getBestHeroes(ApiCallback<List<BestHero>> callback, int[] heroIds, int limit) {
        Map<String, String> options = new TreeMap<>();
        options.put("id", getCommaSeparatedHeroIds(heroIds));
        options.put("limit", String.valueOf(limit));
        apiInterface.getBestHeroes(options).enqueue(new RetrofitCallback<List<BestHero>>(callback));
    }

    private String getCommaSeparatedHeroIds(int[] heroes) {
        StringBuilder commaSeparatedIds = new StringBuilder();
        for (int i = 0; i < heroes.length; i++) {
            if (i == heroes.length - 1) {
                commaSeparatedIds.append(heroes[i]);
            } else {
                commaSeparatedIds.append(heroes[i]).append(",");
            }
        }
        return commaSeparatedIds.toString();
    }
}
