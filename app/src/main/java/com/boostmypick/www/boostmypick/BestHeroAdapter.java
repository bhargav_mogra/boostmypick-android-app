package com.boostmypick.www.boostmypick;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.boostmypick.www.boostmypick.api_client.mappers.BestHero;
import com.boostmypick.www.boostmypick.ui.CircleImageView;
import com.bumptech.glide.Glide;

import java.util.List;

public class BestHeroAdapter extends RecyclerView.Adapter<BestHeroAdapter.ViewHolder> {

    private List<BestHero> bestHeroes;

    public BestHeroAdapter(List<BestHero> bestHeroes) {
        this.bestHeroes = bestHeroes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.best_hero_list_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BestHero item = bestHeroes.get(position);
        if (item == null) {
            return;
        }
        holder.bestHero = item;
        Glide.with(holder.heroImg.getContext()).load(item.getHero().getImageUrl())
                .centerCrop().crossFade()
                .into(holder.heroImg);
        holder.heroName.setText(item.getHero().getName());
        holder.advantage.setText(String.valueOf(item.getAdvantage()));
    }

    @Override
    public int getItemCount() {
        return bestHeroes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public final TextView heroName;
        public final TextView advantage;
        public final CircleImageView heroImg;
        public BestHero bestHero;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            heroName = (TextView) itemView.findViewById(R.id.txt_hero_name);
            advantage = (TextView) itemView.findViewById(R.id.txt_advantage);
            heroImg = (CircleImageView) itemView.findViewById(R.id.hero_img);
        }
    }
}
