package com.boostmypick.www.boostmypick.api_client;

public interface ApiCallback<T> {
    void onSuccess(T object);

    void onFailure(Exception e);
}
