package com.boostmypick.www.boostmypick;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.boostmypick.www.boostmypick.api_client.ApiCallback;
import com.boostmypick.www.boostmypick.api_client.ApiClient;
import com.boostmypick.www.boostmypick.api_client.ApiException;
import com.boostmypick.www.boostmypick.api_client.HeroesAdapter;
import com.boostmypick.www.boostmypick.api_client.OnHeroSelectedListener;
import com.boostmypick.www.boostmypick.api_client.SelectedItemDecoration;
import com.boostmypick.www.boostmypick.api_client.SnackBarUtils;
import com.boostmypick.www.boostmypick.api_client.mappers.BestHero;
import com.boostmypick.www.boostmypick.api_client.mappers.Hero;
import com.boostmypick.www.boostmypick.ui.BezelImageView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnHeroSelectedListener, View.OnClickListener, TextWatcher {
    private ApiClient apiClient;
    private static final int NUMBER_OF_HEROES = 5;
    private List<Hero> heroes = new ArrayList<>();
    private List<BestHero> bestHeroes = new ArrayList<>();
    private SnackBarUtils snackBarUtils;
    private HeroesAdapter heroesAdapter;
    private BezelImageView[] heroImages = new BezelImageView[NUMBER_OF_HEROES];
    private static final int BEST_HERO_LIMIT = 20;
    private LinearLayoutManager heroesListLayoutManager;
    private ProgressBar heroListProgressBar;
    private DrawerLayout drawerLayout;
    private boolean loadingHeroes = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiClient = new ApiClient(BuildConfig.BASE_URL);
        snackBarUtils = new SnackBarUtils();
        EditText searchBar = (EditText) findViewById(R.id.search_bar);
        assert searchBar != null;
        searchBar.addTextChangedListener(this);

        RecyclerView heroesListRecyclerView = (RecyclerView) findViewById(R.id.heroes_list);
        assert heroesListRecyclerView != null;
        heroesAdapter = new HeroesAdapter(heroes, this);
        heroesListLayoutManager = new LinearLayoutManager(this);
        heroesListRecyclerView.setLayoutManager(heroesListLayoutManager);
        heroesListRecyclerView.setAdapter(heroesAdapter);
        heroesListRecyclerView.addItemDecoration(new SelectedItemDecoration(this));

        heroListProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        assert heroListProgressBar != null;

        heroImages[0] = (BezelImageView) findViewById(R.id.img_hero1);
        heroImages[1] = (BezelImageView) findViewById(R.id.img_hero2);
        heroImages[2] = (BezelImageView) findViewById(R.id.img_hero3);
        heroImages[3] = (BezelImageView) findViewById(R.id.img_hero4);
        heroImages[4] = (BezelImageView) findViewById(R.id.img_hero5);

        Button boostMe = (Button) findViewById(R.id.btn_boost);
        assert boostMe != null;
        boostMe.setOnClickListener(this);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawerLayout != null;
        drawerLayout.openDrawer(GravityCompat.START);
        getHeroes();
        FloatingActionButton btnOpenDrawer = (FloatingActionButton) findViewById(R.id.btn_open_drawer);
        assert btnOpenDrawer != null;
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
        ImageButton btnReload = (ImageButton) findViewById(R.id.btn_reload);
        assert btnReload != null;
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHeroes();
            }
        });
    }

    private void getHeroes() {
        if(loadingHeroes)
            return;
        showLoaderForHeroes();
        apiClient.getHeroes(new ApiCallback<Hero[]>() {
            @Override
            public void onSuccess(Hero[] heroes) {
                dismissLoaderForHeroes();
                if (heroes != null && heroes.length > 0) {
                    MainActivity.this.heroes.addAll(Arrays.asList(heroes));
                    heroesAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Exception e) {
                dismissLoaderForHeroes();
                snackBarUtils.alert(MainActivity.this, e.getMessage());
            }
        });
    }

    private void showLoaderForHeroes() {
        loadingHeroes = true;
        heroListProgressBar.setVisibility(View.VISIBLE);
    }

    private void dismissLoaderForHeroes() {
        loadingHeroes = false;
        heroListProgressBar.setVisibility(View.GONE);
    }


    @Override
    public void onHeroSelected(Hero hero) {
        updateImageViews();
    }

    private void updateImageViews() {
        List<Hero> selectedHeroes = heroesAdapter.getSelectedHeroes();
        for (int i = 0, size = NUMBER_OF_HEROES; i < size; i++) {
            if (i >= selectedHeroes.size()) {
                heroImages[i].setImageDrawable(null);
                continue;
            }
            Glide.with(this).load(selectedHeroes.get(i).getImageUrl())
                    .centerCrop().crossFade().into(heroImages[i]);
        }
    }

    @Override
    public void onHeroUnSelected(Hero hero) {
        updateImageViews();
    }

    @Override
    public void onLimitExceeded() {
        snackBarUtils.alert(this, getString(R.string.error_max_hero_selected));
    }

    private void showBestHeroes(List<BestHero> bestHeroes) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.best_heroes_list);
//        dialog.setTitle(R.string.best_heroes_for_you);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.best_hero_list);
        recyclerView.setAdapter(new BestHeroAdapter(bestHeroes));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        int[] heroIds = new int[heroesAdapter.getSelectedHeroes().size()];
        if (v.getId() == R.id.btn_boost) {
            for (int i = 0; i < heroesAdapter.getSelectedHeroes().size(); i++) {
                heroIds[i] = heroesAdapter.getSelectedHeroes().get(i).getId();
            }
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle(R.string.loading);
            progressDialog.setCancelable(false);
            progressDialog.show();
            apiClient.getBestHeroes(new ApiCallback<List<BestHero>>() {
                @Override
                public void onSuccess(List<BestHero> object) {
                    progressDialog.dismiss();
                    bestHeroes.clear();
                    bestHeroes.addAll(object);
                    showBestHeroes(bestHeroes);
                }

                @Override
                public void onFailure(Exception e) {
                    if (e instanceof ApiException) {
                        Log.e("Error", String.valueOf(((ApiException) e).getHttpStatusCode()), e);
                    }
                    progressDialog.dismiss();
                    snackBarUtils.alert(MainActivity.this, e.getMessage());
                }
            }, heroIds, BEST_HERO_LIMIT);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        int position = heroesAdapter.getPositionFor(s);
        if (position == 0) {
            return;
        }
//        heroesListLayoutManager.smoothScrollToPosition(heroesListRecyclerView, null, position);
        heroesListLayoutManager.scrollToPosition(position);
    }
}
