package com.boostmypick.www.boostmypick.api_client;

import com.boostmypick.www.boostmypick.api_client.mappers.Hero;

public interface OnHeroSelectedListener {
    void onHeroSelected(Hero hero);

    void onHeroUnSelected(Hero hero);

    void onLimitExceeded();
}
