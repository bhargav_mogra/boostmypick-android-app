package com.boostmypick.www.boostmypick.api_client;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.boostmypick.www.boostmypick.R;
import com.boostmypick.www.boostmypick.SelectableViewHolder;

/**
 * An item decoration for the selected hero in the hero list.
 */
public class SelectedItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDrawable;

    public SelectedItemDecoration(Context c) {
        this.mDrawable = c.getResources().getDrawable(R.drawable.hero_list_highlight);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            RecyclerView.ViewHolder vh = parent.getChildViewHolder(child);
            if (vh instanceof SelectableViewHolder) {
                if (((SelectableViewHolder) vh).isSelected()) {
                    int left = child.getLeft();
                    int top = child.getTop();
                    int right = child.getRight();
                    int bottom = child.getBottom();
                    mDrawable.setBounds(left, top, right, bottom);
                    mDrawable.draw(c);
                }
            } else {
                throw new RuntimeException("Please implement SelectableViewHolder in your viewholder");
            }
        }
    }
}
