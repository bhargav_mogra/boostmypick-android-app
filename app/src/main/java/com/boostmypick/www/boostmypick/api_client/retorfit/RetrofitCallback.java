package com.boostmypick.www.boostmypick.api_client.retorfit;

import android.util.Log;

import com.boostmypick.www.boostmypick.api_client.ApiCallback;
import com.boostmypick.www.boostmypick.api_client.ApiException;

import retrofit2.Call;
import retrofit2.Response;

public class RetrofitCallback<T> implements retrofit2.Callback<T> {
    private ApiCallback<T> callback;

    public RetrofitCallback(ApiCallback<T> callback) {
        this.callback = callback;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
//        Log.i("Error", "Request URL: " + call.request().url());
        if (response.isSuccessful()) {
            callback.onSuccess(response.body());
        } else {
            /*Log.e("Error", "Request URL: " + call.request().url());
            Log.e("Error", "Error body:" + response.errorBody());*/
            callback.onFailure(new ApiException(response.code()));
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        callback.onFailure(new Exception(t));
    }
}
