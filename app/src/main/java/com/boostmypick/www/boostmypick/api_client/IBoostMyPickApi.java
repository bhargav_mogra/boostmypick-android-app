package com.boostmypick.www.boostmypick.api_client;

import com.boostmypick.www.boostmypick.api_client.mappers.BestHero;
import com.boostmypick.www.boostmypick.api_client.mappers.Hero;

import java.io.IOException;
import java.util.List;

public interface IBoostMyPickApi {
    List<Hero> getHeroes() throws IOException;

    void getHeroes(ApiCallback<Hero[]> callback);

    void getBestHeroes(ApiCallback<List<BestHero>> callback, int[] heroIds, int limit);

    List<BestHero> getBestHeroes(int[] heroIds, int limit) throws IOException;
}
