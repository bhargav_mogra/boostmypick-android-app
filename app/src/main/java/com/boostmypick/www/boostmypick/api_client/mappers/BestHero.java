package com.boostmypick.www.boostmypick.api_client.mappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BestHero {
    @SerializedName("hero")
    @Expose
    private Hero hero;
    @SerializedName("advantage")
    @Expose
    private Float advantage;

    public Hero getHero() {
        return hero;
    }

    public void setHeroId(Hero heroId) {
        this.hero = heroId;
    }

    public Float getAdvantage() {
        return advantage;
    }

    public void setAdvantage(Float advantage) {
        this.advantage = advantage;
    }
}
