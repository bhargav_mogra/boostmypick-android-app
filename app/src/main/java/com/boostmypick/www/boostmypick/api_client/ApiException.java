package com.boostmypick.www.boostmypick.api_client;

import java.io.IOException;

public class ApiException extends IOException {
    private int httpStatusCode;
    public ApiException(int httpStatusCode) {
        init(httpStatusCode);
    }

    private void init(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }


    public ApiException(String detailMessage, int httpStatusCode) {
        super(detailMessage);
        init(httpStatusCode);
    }

    public ApiException(String message, Throwable cause, int httpStatusCode) {
        super(message, cause);
        init(httpStatusCode);
    }

    public ApiException(Throwable cause, int httpStatusCode) {
        super(cause);
        init(httpStatusCode);
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }
}
