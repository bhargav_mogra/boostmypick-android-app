package com.boostmypick.www.boostmypick.api_client.retorfit;

import android.util.Log;

import com.boostmypick.www.boostmypick.BuildConfig;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

public class RetrofitModule {
    private RetrofitApiInterface retrofitApiInterface;

    public RetrofitModule(String baseUrl) {
        Retrofit retrofit = provideRetrofit(baseUrl);
        this.retrofitApiInterface = provideRetrofitInterface(retrofit);
    }

    public RetrofitApiInterface retrofitApiInterface() {
        return this.retrofitApiInterface;
    }

    private Retrofit provideRetrofit(String baseUrl) {
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory
                .create(new GsonBuilder().serializeNulls().excludeFieldsWithoutExposeAnnotation()
                        .create());
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(
                    new HttpLoggingInterceptor.Logger() {
                        @Override
                        public void log(String message) {
                            Log.d("API", message);
                        }
                    }
            );
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            return new Retrofit.Builder().baseUrl(baseUrl)
                    .addConverterFactory(gsonConverterFactory)
                    .client(new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build())
                    .build();
        } else {
            return new Retrofit.Builder().baseUrl(baseUrl)
                    .addConverterFactory(gsonConverterFactory)
                    .client(new OkHttpClient.Builder().build())
                    .build();
        }
    }

    private RetrofitApiInterface provideRetrofitInterface(Retrofit retrofit) {
        return retrofit.create(RetrofitApiInterface.class);
    }
}
