package com.boostmypick.www.boostmypick.api_client;

import com.boostmypick.www.boostmypick.api_client.mappers.BestHero;
import com.boostmypick.www.boostmypick.api_client.mappers.Hero;
import com.boostmypick.www.boostmypick.api_client.retorfit.RetrofitBoostMyPickApi;

import java.io.IOException;
import java.util.List;

public class ApiClient {
    IBoostMyPickApi api;

    public ApiClient(String baseUrl) {
        this.api = new RetrofitBoostMyPickApi(baseUrl);
    }

    public List<Hero> getHeroes() throws IOException {
        return api.getHeroes();
    }

    public void getHeroes(ApiCallback<Hero[]> callback) {
        api.getHeroes(callback);
    }

    public void getBestHeroes(ApiCallback<List<BestHero>> callback, int[] heroIds, int limit) {
        api.getBestHeroes(callback, heroIds, limit);
    }
}
