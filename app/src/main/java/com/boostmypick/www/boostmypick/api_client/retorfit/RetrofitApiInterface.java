package com.boostmypick.www.boostmypick.api_client.retorfit;

import com.boostmypick.www.boostmypick.api_client.mappers.BestHero;
import com.boostmypick.www.boostmypick.api_client.mappers.Hero;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface RetrofitApiInterface {
    @GET("/hero")
    Call<Hero[]> getHeroes();

    @GET("/best-pick")
    Call<List<BestHero>> getBestHeroes(@QueryMap Map<String, String> options);
}
